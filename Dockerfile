FROM node:16-alpine
WORKDIR /node-app
COPY . .
RUN npm install
RUN apk update
EXPOSE 3000
CMD ["node", "/node-app/index.js"]